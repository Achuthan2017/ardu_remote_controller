#ifndef __myproject_h__
#define __myproject_h__

#include <Wire.h> 
#include <SPI.h>
#include <NRFLite.h>

#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#include <Joystick.h>
#include <AxisJoystick.h>

typedef enum
{
  RF_ONLINE,
  RF_OFFLINE,
  RF_NOACK,
  RF_ERROR
}rfStatus_t;

typedef enum
{
  SYS_OK,
  SYS_ERROR
}systemStatus_t;

typedef struct rcConf_{
    rfStatus_t rfStatus;
    bool rfRPD;
    uint32_t beaconSeq;
    systemStatus_t sysStatus;
    uint16_t pot_1SensorValue;
    uint16_t pot_2SensorValue;
    uint8_t joyRight_Dir;
    uint8_t joyLeft_Dir;
    uint16_t motor1_tmp;
    uint16_t motor2_tmp;
} rcStatus_t;

extern rcStatus_t rcStatus;


//******************** BUTTON ****************

extern uint8_t sw_2;

//*******************************************

extern uint8_t joyRightVRX;
extern uint8_t joyRightVRY;
extern uint8_t joyLeftVRX;
extern uint8_t joyLeftVRY;

extern uint8_t led_1;
extern uint8_t led_2;


bool getPotValues( void );
void oledInit(void);
void oledWrite(uint8_t x, uint8_t y, const String& txt);
void readButtons( void );
void updateOled(void);

#endif
