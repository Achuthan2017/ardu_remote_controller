#include "remoteController.h"


// macro for detection of rising edge and debouncing
/*the state argument (which must be a variable) records the current
  and the last 7 reads by shifting one bit to the left at each read.
  If the value is 15(=0b00001111) we have one rising edge followed by
  4 consecutive 1's. That would qualify as a debounced rising edge*/
#define DRE(signal, state) (state=(state<<1)|signal)==B00001111

// Rising state variables for each button
byte button1RisingState;

// macro for detection of falling edge and debouncing
/*the state argument (which must be a variable) records the current
  and the last 7 reads by shifting one bit to the left at each read.
  If the value is 240(=0b11110000) we have one falling edge followed by
  4 consecutive 0's. That would qualify as a debounced falling edge*/
#define DFE(signal, state) (state=(state<<1)|signal)==B11110000

// Falling state variables for each button
byte button1FallingState;


void readButtons( void ) 
{
  // Read button states every 5 ms (debounce time):
  static unsigned long lastDebounce;
  if (millis() - lastDebounce >= 5) {
    lastDebounce = millis();

    // Rising edge (if switch is released)
    if (DRE(digitalRead(sw_2), button1RisingState)) {
      Serial.print ("Rising edge (pulled high by internal pullup resistor). State variable: ");
      Serial.println (button1RisingState);
      
    }

    // Falling edge (if switch is pressed)
    if (DFE(digitalRead(sw_2), button1FallingState)) {
      Serial.print ("Falling edge (pulled low by switch). State variable: ");
      Serial.println (button1FallingState);

      /*
      oledWrite(30, 20, "Falling edge");

      uint16_t pot_1SensorValue;
      uint16_t pot_2SensorValue;

      getPotValues(&pot_1SensorValue, &pot_2SensorValue);

      String msg = "pot1 ";
      msg += pot_1SensorValue;
      msg += " pot2 ";
      msg += pot_2SensorValue;

      Serial.println (msg);
      oledWrite(30, 30, msg);
      */
    }
  }
}
