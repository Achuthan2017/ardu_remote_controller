#include "remoteController.h"

//******************** OLED ****************
#define OLED_ADDR   0x3C
Adafruit_SSD1306 display(-1);
#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif
//*******************************************

void oledInit(void)
{
  // initialize and clear display
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  display.clearDisplay();
  display.display();

  // display a line of text
  display.setTextSize(1);
  display.setTextColor(WHITE);
}

void oledWrite(uint8_t x, uint8_t y, const String& txt)
{
  
  display.setCursor(x, y);
  display.print(txt);
  display.display();
}


void updateOled(void)
{
  String msg;

  display.clearDisplay();
  display.setCursor(12,0);
  display.print("Optimus RF-RC V2");

  msg = "Sys status: ";
  msg += rcStatus.sysStatus;
  display.setCursor(0,10);
  display.print(msg);

  msg = "RF status: ";
  msg += rcStatus.rfStatus;
  msg += " ";
  msg += rcStatus.rfRPD;
  msg += " ";
  msg += rcStatus.beaconSeq;
  display.setCursor(0,20);
  display.print(msg);
  //Serial.println(msg);

  msg = "Pots: ";
  msg += rcStatus.pot_1SensorValue;
  msg += " | ";
  msg += rcStatus.pot_2SensorValue;
  display.setCursor(0, 30);
  display.print( msg);

/************************************/
  msg = "Left ";
  msg += rcStatus.joyLeft_Dir;
  msg += " Right ";
  msg += rcStatus.joyRight_Dir;
  
  if( rcStatus.joyRight_Dir == 2  )
  {
    msg += " Up";
  }
  else if( rcStatus.joyRight_Dir == 1  )
  {
    msg += " Down";
  }
  else if( rcStatus.joyRight_Dir == 4 )
  {
    msg += " Right";
  }
  else if( rcStatus.joyRight_Dir == 3 )
  {
    msg += " Left";
  }
  else if ( rcStatus.joyRight_Dir == 5 )
  {
    msg += " Stop";
  }
  
  display.setCursor(0, 40);
  display.print( msg);
  
  /************************************/
  msg = " t1 ";
  msg += rcStatus.motor1_tmp;
  msg += " t2 ";
  msg += rcStatus.motor2_tmp;

  display.setCursor(0, 50);
  display.print( msg);

/************************************/
  display.display();
}
