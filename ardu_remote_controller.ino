#include "remoteController.h"

rcStatus_t rcStatus;

//******************** RADIO ****************
const static uint8_t RADIO_ID = 1;
const static uint8_t DESTINATION_RADIO_ID = 0;
const static uint8_t PIN_RADIO_CE = 7;
const static uint8_t PIN_RADIO_CSN = 8;
const static uint8_t PIN_RADIO_IRQ = 2;

enum RadioPacketType
{
    Heartbeat,
    BeginGetData,
    EndGetData,
    ReceiverData
};

struct RadioPacket
{
  RadioPacketType PacketType;
  uint8_t FromRadioId;
  uint8_t Direction;
  uint8_t Speed;
  uint8_t Motor1Tmp;
  uint8_t Motor2Tmp;
  uint32_t Uptime;
};

NRFLite _radio;
volatile uint8_t _dataWasReceived;
volatile uint32_t lastHeartBeatSeq;
uint32_t controlHB;
bool controlFlag = false;

//*******************************************

uint16_t pot_1 = A0;
uint16_t pot_2 = A1;

uint8_t sw_2  = 4;
uint8_t joyRightVRX = A3;
uint8_t joyRightVRY = A2;
uint8_t joyLeftVRX = A6;
uint8_t joyLeftVRY = A7;

extern uint8_t led_1 = 5;
extern uint8_t led_2 = 6;

bool sw_2Press = false;

Joystick* joystick_right;

static uint8_t joyStateIdle = 0;

void sendRFdata(RadioPacket radioData);

void setup() {
  
   Serial.begin(115200); 
   
   pinMode(pot_1, INPUT);
   pinMode(pot_2, INPUT);

   pinMode(led_1, OUTPUT);
   pinMode(led_2, OUTPUT);
   
   pinMode(sw_2, INPUT_PULLUP);

  oledInit();

  digitalWrite(led_1, HIGH);
  digitalWrite(led_2, HIGH);

   if (!_radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN))
   {
     Serial.println("Cannot communicate with radio");
     while (1); // Wait here forever.
   }
  attachInterrupt(digitalPinToInterrupt(PIN_RADIO_IRQ), radioInterrupt, FALLING);

  rcStatus.rfStatus = RF_ONLINE;
  rcStatus.sysStatus = SYS_OK;

  joystick_right = new AxisJoystick(1, joyRightVRX, joyRightVRY);

  updateOled();
}

void loop() {
  
  //readButtons();
  getPotValues();
  getJoyValues(joystick_right->multipleRead());
  
  if (_dataWasReceived)
  {
    _dataWasReceived = false;
    while (_radio.hasDataISR())
    {
      RadioPacket radioData;
      _radio.readData(&radioData);
      lastHeartBeatSeq = radioData.Uptime;
      bool rpd = _radio.getRPD();
      rcStatus.beaconSeq = lastHeartBeatSeq;
      rcStatus.rfRPD = rpd;
      rcStatus.motor1_tmp = radioData.Motor1Tmp;
      rcStatus.motor2_tmp = radioData.Motor2Tmp;
      updateOled();
    }
  }
}

void getJoyValues(const Joystick::Move move)
{
  
  RadioPacket  heartBeatData;
  heartBeatData.PacketType = Heartbeat;
  heartBeatData.FromRadioId = RADIO_ID;

  switch (move) {
    case Joystick::Move::NOT:
      rcStatus.joyRight_Dir = 5;  
      heartBeatData.Direction =  5;
      heartBeatData.Speed = rcStatus.pot_1SensorValue;
      if(joyStateIdle != 1)
      {
        sendRFdata(heartBeatData);
        joyStateIdle = 1;
        //updateOled();
      }
      break;
    case Joystick::Move::PRESS:
      break;
    case Joystick::Move::UP:
      rcStatus.joyRight_Dir = 1;  
      heartBeatData.Direction =  1;
      heartBeatData.Speed = rcStatus.pot_1SensorValue;
      if(joyStateIdle != 2)
      {
        sendRFdata(heartBeatData);
        joyStateIdle = 2;
        //updateOled();
      }
      break;
    case Joystick::Move::DOWN:
      rcStatus.joyRight_Dir = 2;  
      heartBeatData.Direction =  2;
      heartBeatData.Speed = rcStatus.pot_1SensorValue;
      if(joyStateIdle != 3)
      {
        sendRFdata(heartBeatData);
        joyStateIdle = 3;
        //updateOled();
      }
      break;
    case Joystick::Move::RIGHT:
      rcStatus.joyRight_Dir = 4;  
      heartBeatData.Direction =  4;
      heartBeatData.Speed = rcStatus.pot_1SensorValue;
      if(joyStateIdle != 4)
      {
        sendRFdata(heartBeatData);
        joyStateIdle = 4;
        //updateOled();
      }
      break;
    case Joystick::Move::LEFT:
      rcStatus.joyRight_Dir = 3;  
      heartBeatData.Direction =  3;
      heartBeatData.Speed = rcStatus.pot_1SensorValue;
      if(joyStateIdle != 5)
      {
        sendRFdata(heartBeatData);
        joyStateIdle = 5;
        //updateOled();
      }
      break;
    default:

      break;
  }

  
  
}

bool getPotValues( void )
{
  bool returnCode = false;
  
  uint16_t pot_1NewValue = map(analogRead(pot_1), 0, 1023, 0, 255); 
  uint16_t pot_2NewValue = map(analogRead(pot_2), 0, 1023, 0, 255); 
  
  if ( pot_1NewValue < (rcStatus.pot_1SensorValue-5) ||  
       pot_1NewValue > (rcStatus.pot_1SensorValue+5)  )
  {
    rcStatus.pot_1SensorValue = pot_1NewValue;
    returnCode = true;
  }

  if ( pot_2NewValue < (rcStatus.pot_2SensorValue-5) ||  
       pot_2NewValue > (rcStatus.pot_2SensorValue+5)  )
  {
    rcStatus.pot_2SensorValue = pot_2NewValue;
    returnCode = true;
  }

  if( returnCode )
  {
    updateOled();
  }
  
  return returnCode;
  
}

void sendRFdata(RadioPacket radioData)
{
  if (_radio.send(DESTINATION_RADIO_ID, &radioData, sizeof(radioData)))
        {
            Serial.println("...Success");
            rcStatus.rfStatus = RF_ONLINE;
        }
        else
        {
            Serial.println("...Failed");
            rcStatus.rfStatus = RF_NOACK;
        }
        _radio.startRx();
}

void radioInterrupt()
{
    // Ask the radio what caused the interrupt.  This also resets the IRQ pin on the
    // radio so a new interrupt can be triggered.

    uint8_t txOk, txFail, rxReady;
    _radio.whatHappened(txOk, txFail, rxReady);

    // txOk = the radio successfully transmitted data.
    // txFail = the radio failed to transmit data.
    // rxReady = the radio received data.

    if (rxReady)
    {
        _dataWasReceived = true;
    }
}
